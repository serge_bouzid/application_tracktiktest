Application_TrackTikTest
========================



Description
-----------

Application TrackTik test

---



Requirement
-----------

- Script language: PHP: version 5.6 || 7

---



Installation
------------

Several ways are possible:

#### Git (preferred)

1. Command: Move in project parent directory path
    
    ```sh
    cd "<project_parent_dir_path>"
    ```

2. Command: Installation
    
    ```sh
    git clone https://bitbucket.org/serge_bouzid/application_tracktiktest.git
    ```

3. Command: Install external libraries
    
    ```sh
    php composer.phar install
    ```

#### Download

1. Download: Get repository from "https://bitbucket.org/serge_bouzid/application_tracktiktest/downloads/"

2. Operation: Move downloaded repository to project parent directory

3. Operation: Rename project

4. Command: Install external libraries

    ```sh
    php composer.phar install
    ```

---



Usage
---------------------

- Get answer for question 1

    ```sh
    php bin/app scenario-1
    ```
    
- Get answer for question 2
    ```sh
    php bin/app scenario-2
    ```

---


