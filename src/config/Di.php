<?php

use application\tracktik_test\electronic\model\ItemCollection;
use application\tracktik_test\electronic\factory\ItemFactory;

return array(
    /**
     * // Dependency 1
     * 'string dependency 1 key' (required) => [
     *     callback (required) => closure,
     *     shared (optional: got false if not found) => true / false
     * ],
     *
     * ...,
     *
     * // Dependency N
     * 'string dependency N key' => [...]
     */

    // Electronic item dependencies
    // ******************************************************************************

    ItemCollection::class => [
        'callback' => function()
        {
            return new ItemCollection(array());
        }
    ],

    ItemFactory::class => [
        'callback' => function()
        {
            return new ItemFactory();
        },
        'shared' => true
    ]
);