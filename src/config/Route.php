<?php

use application\tracktik_test\electronic\controller\ItemController;

return array(
    /**
     * // Route 1
     * 'string route 1' (required) => [
     *     class_path (required) => 'string controller class path',
     *     method_name (optional: got 'actionIndex' if not found) => 'string method name'
     * ],
     *
     * ...,
     *
     * // Route N
     * 'string route N' => [...]
     */

    // Electronic item routes
    // ******************************************************************************

    'scenario-1' => [
        'class_path' => ItemController::class,
        'method_name' => 'actionScenario1'
    ],

    'scenario-2' => [
        'class_path' => ItemController::class,
        'method_name' => 'actionScenario2'
    ]
);