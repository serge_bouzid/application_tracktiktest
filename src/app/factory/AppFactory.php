<?php
/**
 * Description :
 * This class allows to define application factory class.
 * 
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\app\factory;

use application\tracktik_test\app\model\Provider;
use application\tracktik_test\app\model\Router;
use application\tracktik_test\app\model\App;

class AppFactory
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * Get new provider.
     *
     * @return Provider
     */
    public static function getProvider()
    {
        return new Provider();
    }



    /**
     * Get new router.
     *
     * @return Router
     */
    public static function getRouter()
    {
        return new Router();
    }



    /**
     * Get new application.
     *
     * @return App
     */
    public static function getApp()
    {
        return new App(
            static::getProvider(),
            static::getRouter()
        );
    }
}