<?php
/**
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\app\library;

class ToolBoxRender
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * Get render,
     * from specified data array.
     *
     * @param array $data
     * @return string
     */
    public static function getRenderFromData(array $data)
    {
        return json_encode($data, JSON_PRETTY_PRINT);
    }
}