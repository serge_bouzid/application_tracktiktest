<?php
/**
 * Description :
 * This class allows to define application class.
 * 
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\app\model;

use Exception;
use application\tracktik_test\app\model\Provider;
use application\tracktik_test\app\model\Router;

class App
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

    /** @var Provider */
    protected $provider;



    /** @var Router */
    protected $router;



    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * Constructor.
     *
     * @param Provider $provider
     * @param Router $router
     */
    public function __construct(
        Provider $provider,
        Router $router
    )
    {
        // Set properties
        $this->setProvider($provider);
        $this->setRouter($router);
    }



    /**
     * Get provider.
     *
     * @return Provider
     */
    public function getProvider()
    {
        return $this->provider;
    }



    /**
     * Get router.
     *
     * @return Router
     */
    public function getRouter()
    {
        return $this->router;
    }



    /**
     * Get specified dependency.
     *
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->provider->get($key);
    }



    /**
     * Get route.
     *
     * @return string
     * @throws Exception
     */
    public function getRoute()
    {
        $result = (
            (isset($_SERVER['argv'][1]) && is_string($_SERVER['argv'][1])) ?
                $_SERVER['argv'][1] :
                null
        );

        // Check route provided
        if(is_null($result))
        {
            throw new Exception('No route provided!');
        }

        return $result;
    }



    /**
     * Set specified provider.
     *
     * @param Provider $provider
     */
    public function setProvider(Provider $provider)
    {
        $this->provider = $provider;
    }



    /**
     * Set specified router.
     *
     * @param Router $router
     */
    public function setRouter(Router $router)
    {
        $this->router = $router;
    }



    /**
     * Run application.
     *
     * @return null|mixed
     */
    public function run()
    {
        $route = $this->getRoute();

        return $this->router->execute(
            $route,
            array($this)
        );
    }
}