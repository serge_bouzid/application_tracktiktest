<?php
/**
 * Description :
 * This class allows to define provider class.
 * 
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\app\model;

use Exception;

class Provider
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

    /** @var array */
    protected $tabConfig;



    /** @var array */
    protected $tabInstance;



    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * Constructor.
     */
    public function __construct()
    {
        // Set properties
        $this->tabConfig = array();
        $this->tabInstance = array();
    }



    /**
     * Check if specified dependency exists.
     *
     * @param string $key
     * @return boolean
     */
    public function checkDependencyExists($key)
    {
        $tabConfig = $this->getTabConfig();
        return (
            is_string($key) &&
            array_key_exists($key, $tabConfig)
        );
    }



    /**
     * Set check specified dependency configuration.
     *
     * @param mixed $config
     * @throws Exception
     */
    protected function setCheckDependencyConfig($config)
    {
        $result =
            is_array($config) &&

            isset($config['callback']) &&
            is_callable($config['callback']) &&

            (
                (!isset($config['shared'])) ||
                is_bool($config['shared'])
            );

        if(!$result)
        {
            throw new Exception('Dependency configuration invalid!');
        };
    }



    /**
     * Get configuration.
     *
     * @return array
     */
    protected function getTabConfig()
    {
        // Get configuration, from file config, if required
        if(count($this->tabConfig) == 0)
        {
            $this->tabConfig = require(dirname(__FILE__) . '/../../config/Di.php');
        }

        return $this->tabConfig;
    }



    /**
     * Get specified dependency configuration.
     *
     * @param string $key
     * @return null|array
     * @throws Exception
     */
    protected function getConfig($key)
    {
        $tabConfig = $this->getTabConfig();
        $result = (
            $this->checkDependencyExists($key) ?
                $tabConfig[$key] :
                null
        );

        // Check dependency configuration valid, if required
        if(!is_null($result)) $this->setCheckDependencyConfig($result);

        return $result;
    }



    /**
     * Get specified dependency.
     *
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public function get($key)
    {
        $config = $this->getConfig($key);

        // Check dependency found
        if(is_null($config))
        {
            throw new Exception('Dependency not found!');
        }

        // Get dependency
        $share = (isset($config['shared']) && $config['shared']);
        $result = (
            (
                $share &&
                array_key_exists($key, $this->tabInstance)
            ) ?
                $this->tabInstance[$key] :
                $config['callback']()
        );

        // Register dependency, if required
        if($share) $this->tabInstance[$key] = $result;

        return $result;
    }
}