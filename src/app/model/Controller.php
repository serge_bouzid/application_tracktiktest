<?php
/**
 * Description :
 * This class allows to define base controller class.
 * 
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\app\model;

abstract class Controller
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

    /** @var App */
    protected $app;



    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * Constructor.
     *
     * @param App $app
     */
    public function __construct(App $app)
    {
        // Set properties
        $this->app = $app;
    }
}