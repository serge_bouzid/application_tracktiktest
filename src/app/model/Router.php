<?php
/**
 * Description :
 * This class allows to define router class.
 * 
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\app\model;

use Exception;
use ReflectionClass;
use application\tracktik_test\app\model\Controller;

class Router
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

    /** @var array */
    protected $tabConfig;



    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * Constructor.
     */
    public function __construct()
    {
        // Set properties
        $this->tabConfig = array();
    }



    /**
     * Check if specified route exists.
     *
     * @param string $route
     * @return boolean
     */
    public function checkRouteExists($route)
    {
        $tabConfig = $this->getTabConfig();
        return (
            is_string($route) &&
            array_key_exists($route, $tabConfig)
        );
    }



    /**
     * Set check specified route configuration.
     *
     * @param mixed $config
     * @throws Exception
     */
    protected function setCheckRouteConfig($config)
    {
        $result =
            is_array($config) &&

            isset($config['class_path']) &&
            is_string($config['class_path']) &&
            is_subclass_of($config['class_path'], Controller::class) &&

            (
                (!isset($config['method_name'])) ||
                is_string($config['method_name'])
            );

        if(!$result)
        {
            throw new Exception('Route configuration invalid!');
        };
    }



    /**
     * Get configuration.
     *
     * @return array
     */
    protected function getTabConfig()
    {
        // Get configuration, from file config, if required
        if(count($this->tabConfig) == 0)
        {
            $this->tabConfig = require(dirname(__FILE__) . '/../../config/Route.php');
        }

        return $this->tabConfig;
    }



    /**
     * Get specified route configuration.
     *
     * @param string $route
     * @return null|array
     * @throws Exception
     */
    protected function getConfig($route)
    {
        $tabConfig = $this->getTabConfig();
        $result = (
            $this->checkRouteExists($route) ?
                $tabConfig[$route] :
                null
        );

        // Check route configuration valid, if required
        if(!is_null($result)) $this->setCheckRouteConfig($result);

        return $result;
    }



    /**
     * Execute specified route.
     *
     * @param string $route
     * @param array $constructArg = array()
     * @return null|mixed
     * @throws Exception
     */
    public function execute(
        $route,
        array $constructArg = array()
    )
    {
        $config = $this->getConfig($route);

        // Check route found
        if(is_null($config))
        {
            throw new Exception('Route not found!');
        }

        // Call method
        $classPath = $config['class_path'];
        $methodNm = $config['method_name'];
        $class = new ReflectionClass($classPath);
        $result = $class
            ->getMethod($methodNm)
            ->invoke($class->newInstanceArgs($constructArg));

        return $result;
    }
}