<?php
/**
 * Description :
 * This class allows to define item class.
 *
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\electronic\model;

use Exception;
use ReflectionClass;

abstract class Item
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

    /** @var integer|float */
    protected $price;

	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    /**
     * Constructor.
     *
     * @param integer|float $price
     */
    public function __construct($price)
    {
        // Set properties
        $this->setPrice($price);
    }



    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return (new ReflectionClass($this))->getShortName();
    }



    /**
     * Get price.
     *
     * @return integer|float
     */
    public function getPrice()
    {
        return $this->price;
    }



    /**
     * Set specified price.
     *
     * @param integer|float $price
     * @throws Exception
     */
    public function setPrice($price)
    {
        if(
            (
                (!is_int($price)) &&
                (!is_float($price))
            ) ||
            ($price < 0)
        )
        {
            throw new Exception('Price invalid, it must be a postive numeric value!');
        }

        $this->price = $price;
    }
}