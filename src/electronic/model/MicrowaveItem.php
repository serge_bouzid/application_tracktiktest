<?php
/**
 * Description :
 * This class allows to define microwave item class.
 * 
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\electronic\model;

use application\tracktik_test\electronic\model\Item;

class MicrowaveItem extends Item
{

}