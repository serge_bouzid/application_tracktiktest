<?php
/**
 * Description :
 * This class allows to define extra item class.
 * 
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\electronic\model;

use application\tracktik_test\electronic\model\Item;

use Exception;
use application\tracktik_test\electronic\model\ItemCollection;

abstract class ExtraItem extends Item
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

    /**
     * Max count of extra items
     * If <= 0 : no limit of extra items
     * @var int
     */
    static protected $__extraItemCountMax = -1;



    /** @var ItemCollection */
    protected $extraItemCollection;



    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ItemCollection $extraItemCollection
     */
    public function __construct(
        $price,
        $extraItemCollection
    )
    {
        parent::__construct($price);

        // Set properties
        $this->setExtraItemCollection($extraItemCollection);
    }



    /**
     * Check extra item count maximum.
     *
     * @param integer $countNew = 0
     * @param ItemCollection $extraItemCollection = null
     * @return boolean
     */
    protected function checkExtraItemCountMax(
        $countNew = 0,
        ItemCollection $extraItemCollection = null
    )
    {
        $extraItemCollection = (
            is_null($extraItemCollection) ?
                $this->extraItemCollection :
                $extraItemCollection
        );
        return (
            (static::$__extraItemCountMax < 0) ||
            (
                (
                    count($extraItemCollection->getTabItem()) +
                    ($countNew > 0 ? $countNew : 0)
                ) <= static::$__extraItemCountMax
            )
        );
    }



    /**
     * Set check extra item count maximum.
     *
     * @param integer $countNew = 0
     * @param ItemCollection $extraItemCollection = null
     * @throws Exception
     */
    protected function setCheckExtraItemCountMax(
        $countNew = 0,
        ItemCollection $extraItemCollection = null
    )
    {
        if(!$this->checkExtraItemCountMax($countNew, $extraItemCollection))
        {
            throw new Exception(sprintf(
                'Maximum count of item (%1$s) exceeded!',
                static::$__extraItemCountMax
            ));
        };
    }



    /**
     * Check if specified item exists.
     *
     * @param Item $item
     * @return boolean
     */
    public function checkExtraItemExists(Item $item)
    {
        return $this->extraItemCollection->checkItemExists($item);
    }



    /**
     * Get index array of extra items.
     *
     * Index array of criteria item class paths:
     * @see ItemCollection::getTabItem() Index array of criteria item class paths.
     *
     * Sorting price asc format:
     * @see ItemCollection::getTabItem() Sorting price asc format.
     *
     * @param string[] $tabCritItemClassPath
     * @param null|boolean $sortPriceAsc = null
     * @return Item[]
     */
    public function getTabExtraItem(
        array $tabCritItemClassPath = array(),
        $sortPriceAsc = null
    )
    {
        return $this->extraItemCollection->getTabItem(
            $tabCritItemClassPath,
            $sortPriceAsc
        );
    }



    /**
     * Get price of extra items.
     *
     * Index array of criteria item class paths:
     * @see ItemCollection::getTabItem() Index array of criteria item class paths.
     *
     * @param string[] $tabCritItemClassPath
     * @return integer|float
     */
    public function getExtraPrice(
        array $tabCritItemClassPath = array()
    )
    {

        return $this->extraItemCollection->getPrice($tabCritItemClassPath);
    }



    /**
     * Get single price.
     *
     * @return integer|float
     */
    public function getSinglePrice()
    {

        return parent::getPrice();
    }



    /**
     * @inheritdoc
     */
    public function getPrice()
    {

        return ($this->getSinglePrice() + $this->getExtraPrice());
    }



    /**
     * Set specified extra item collection.
     *
     * @param ItemCollection $extraItemCollection
     * @throws Exception
     */
    public function setExtraItemCollection(ItemCollection $extraItemCollection)
    {
        $this->setCheckExtraItemCountMax(0, $extraItemCollection);
        $this->extraItemCollection = $extraItemCollection;
    }



    /**
     * Set specified item(s).
     *
     * @param Item|Item[] $item
     * @throws Exception
     */
    public function setExtraItem($item)
    {
        $countNew = (is_array($item) ? count($item) : 1);
        $this->setCheckExtraItemCountMax($countNew);
        $this->extraItemCollection->setItem($item);
    }



    /**
     * Remove specified extra item(s).
     *
     * @param Item|Item[] $item
     */
    public function removeExtraItem($item)
    {
        $this->extraItemCollection->removeItem($item);
    }



    /**
     * Remove all extra items.
     */
    public function removeExtraItemAll()
    {
        $this->extraItemCollection->removeItemAll();
    }
}