<?php
/**
 * Description :
 * This class allows to define console item class.
 * 
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\electronic\model;

use application\tracktik_test\electronic\model\ExtraItem;

class ConsoleItem extends ExtraItem
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

    /**
     * Max count of extra items
     * If <= 0 : no limit of extra items
     * @var int
     */
    static protected $__extraItemCountMax = 4;
}