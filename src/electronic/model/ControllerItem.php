<?php
/**
 * Description :
 * This class allows to define controller item class.
 * 
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\electronic\model;

use application\tracktik_test\electronic\model\Item;

use Exception;

class ControllerItem extends Item
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

    /** @var boolean */
    protected $wired;



    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param boolean $wired
     */
    public function __construct(
        $price,
        $wired
    )
    {
        parent::__construct($price);

        // Set properties
        $this->setIsWired($wired);
    }



    /**
     * Check if controller is wired.
     *
     * @return boolean
     */
    public function checkIsWired()
    {
        return $this->wired;
    }



    /**
     * Set specified wired status.
     *
     * @param boolean $wired
     * @throws Exception
     */
    public function setIsWired($wired)
    {
        if(!is_bool($wired))
        {
            throw new Exception('Wired status invalid, it must be a boolean value!');
        }

        $this->wired = $wired;
    }
}