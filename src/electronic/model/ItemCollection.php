<?php
/**
 * Description :
 * This class allows to define item collection class.
 *
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\electronic\model;

use Exception;
use application\tracktik_test\electronic\model\Item;

class ItemCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

    /** @var Item[] */
    protected $tabItem;

	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    /**
     * Constructor.
     *
     * @param Item[] $tabItem
     */
    public function __construct(array $tabItem)
    {
        // Set properties
        $this->tabItem = array();
        $this->setItem($tabItem);
    }



    /**
     * Check if specified item exists.
     *
     * @param Item $item
     * @return boolean
     */
    public function checkItemExists(Item $item)
    {
        return in_array($item, $this->tabItem, true);
    }



    /**
     * Get index array of items.
     *
     * Index array of criteria item class paths,
     * can be provided as criteria.
     *
     * If sorting price asc is true, it sort price ascending,
     * if false, it sort price descending,
     * if null, no price sorting.
     *
     * @param string[] $tabCritItemClassPath
     * @param null|boolean $sortPriceAsc = null
     * @return Item[]
     */
    public function getTabItem(
        array $tabCritItemClassPath = array(),
        $sortPriceAsc = null
    )
    {
        $tabCritItemClassPath = array_values($tabCritItemClassPath);
        $sortPriceAsc = (is_bool($sortPriceAsc) ? $sortPriceAsc : null);

        // Filter items
        $result = array_filter(
            $this->tabItem,
            function(Item $item) use ($tabCritItemClassPath)
            {
                $result = (count($tabCritItemClassPath) == 0);

                for($cpt = 0; ($cpt < count($tabCritItemClassPath)) && (!$result); $cpt++)
                {
                    $itemClassPath = $tabCritItemClassPath[$cpt];
                    $result = ($item instanceof $itemClassPath);
                }

                return $result;
            }
        );

        // Sort price
        if(!is_null($sortPriceAsc))
        {
            usort(
                $result,
                function(Item $item1, Item $item2) use ($sortPriceAsc)
                {
                    $item1Price = $item1->getPrice();
                    $item2Price = $item2->getPrice();

                    $result = (
                        ($item1Price < $item2Price) ?
                            -1 :
                            (($item1Price > $item2Price) ? 1 : 0)
                    );
                    $result = ($sortPriceAsc ? $result : ($result * -1));

                    return $result;
                }
            );
        }

        return $result;
    }



    /**
     * Get price of items.
     *
     * Index array of criteria item class paths:
     * @see getTabItem() Index array of criteria item class paths.
     *
     * @param string[] $tabCritItemClassPath
     * @return integer|float
     */
    public function getPrice(
        array $tabCritItemClassPath = array()
    )
    {
        $result = 0;

        $tabItem = $this->getTabItem($tabCritItemClassPath);
        foreach($tabItem as $item)
        {
            $result += $item->getPrice();
        }

        return $result;
    }



    /**
     * Set specified item(s).
     *
     * @param Item|Item[] $item
     */
    public function setItem($item)
    {
        $tabItem = array_filter(
            (is_array($item) ? array_values($item) : array($item)),
            function($item) {return ($item instanceof Item);}
        );

        // Add items
        foreach($tabItem as $item)
        {
            $this->tabItem[] = $item;
        }
    }



    /**
     * Remove specified item(s).
     *
     * @param Item|Item[] $item
     */
    public function removeItem($item)
    {
        $tabItem = array_filter(
            (is_array($item) ? array_values($item) : array($item)),
            function($item) {return ($item instanceof Item);}
        );

        // Reset items, without selection
        $this->tabItem = array_filter(
            $this->tabItem,
            function($item) use ($tabItem) {
                return (!in_array($item, $tabItem, true));
            }
        );
    }



    /**
     * Remove all items.
     */
    public function removeItemAll()
    {
        $this->tabItem = array();
    }
}