<?php
/**
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\electronic\controller;

use application\tracktik_test\app\model\Controller;

use application\tracktik_test\app\library\ToolBoxRender;
use application\tracktik_test\electronic\library\ToolBoxItemRender;
use application\tracktik_test\electronic\model\ItemCollection;
use application\tracktik_test\electronic\model\ConsoleItem;
use application\tracktik_test\electronic\factory\ItemFactory;

class ItemController extends Controller
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * Get item collection.
     *
     * @return ItemCollection
     */
    protected function getItemCollection()
    {
        /** @var ItemCollection $result */
        $result = $this->app->get(ItemCollection::class);
        /** @var ItemFactory $itemFactory */
        $itemFactory = $this->app->get(ItemFactory::class);

        // Get console item
        /** @var ItemCollection $consoleExtraItemCollection */
        $consoleExtraItemCollection = $this->app->get(ItemCollection::class);
        $consoleExtraItemCollection->setItem(array(
            $itemFactory->getControllerItem(array('price' => 30, 'wired' => false)),
            $itemFactory->getControllerItem(array('price' => 30, 'wired' => false)),
            $itemFactory->getControllerItem(array('price' => 20, 'wired' => true)),
            $itemFactory->getControllerItem(array('price' => 20, 'wired' => true))
        ));
        $consoleItem = $itemFactory->getConsoleItem(array(
            'price' => 400,
            'extra_item_collection' => $consoleExtraItemCollection
        ));

        // Get television 1 item
        /** @var ItemCollection $tv1ExtraItemCollection */
        $tv1ExtraItemCollection = $this->app->get(ItemCollection::class);
        $tv1ExtraItemCollection->setItem(array(
            $itemFactory->getControllerItem(array('price' => 10, 'wired' => false)),
            $itemFactory->getControllerItem(array('price' => 10, 'wired' => false))
        ));
        $tv1Item = $itemFactory->getTelevisionItem(array(
            'price' => 300,
            'extra_item_collection' => $tv1ExtraItemCollection
        ));

        // Get television 2 item
        /** @var ItemCollection $tv2ExtraItemCollection */
        $tv2ExtraItemCollection = $this->app->get(ItemCollection::class);
        $tv2ExtraItemCollection->setItem(array(
            $itemFactory->getControllerItem(array('price' => 10, 'wired' => false))
        ));
        $tv2Item = $itemFactory->getTelevisionItem(array(
            'price' => 350,
            'extra_item_collection' => $tv2ExtraItemCollection
        ));

        // Get microwave item
        $microwaveItem = $itemFactory->getMicrowaveItem(array(
            'price' => 200
        ));

        // Fill item collection
        $result->setItem(array(
            $consoleItem,
            $tv1Item,
            $tv2Item,
            $microwaveItem
        ));

        return $result;
    }



    /**
     * Action scenario 1
     */
    public function actionScenario1()
    {
        // Get item collection
        $itemCollection = $this->getItemCollection();
        $tabItem = $itemCollection->getTabItem(array(), true);

        // Render
        $render = ToolBoxRender::getRenderFromData(
            ToolBoxItemRender::getItemCollectionRenderData($tabItem)
        );
        echo($render);
    }



    /**
     * Action scenario 2
     */
    public function actionScenario2()
    {
        // Get item collection
        $itemCollection = $this->getItemCollection();
        $tabItem = $itemCollection->getTabItem(array(ConsoleItem::class), true);

        // Render
        $render = ToolBoxRender::getRenderFromData(
            ToolBoxItemRender::getItemCollectionRenderData($tabItem)
        );
        echo($render);
    }
}