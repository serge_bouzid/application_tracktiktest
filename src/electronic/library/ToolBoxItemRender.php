<?php
/**
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\electronic\library;

use application\tracktik_test\electronic\model\Item;
use application\tracktik_test\electronic\model\ItemCollection;
use application\tracktik_test\electronic\model\ControllerItem;
use application\tracktik_test\electronic\model\ExtraItem;

class ToolBoxItemRender
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * Get item render data.
     *
     * @param Item $item
     * @return array
     */
    public static function getItemRenderData(Item $item)
    {
        $result = array(
            'type' => $item->getType(),
            'price' => $item->getPrice()
        );

        if($item instanceof ControllerItem)
        {
            $result['wired'] = $item->checkIsWired();
        }

        if($item instanceof ExtraItem)
        {
            $result['price_description'] = sprintf(
                '%1$s (item:%2$s + extras:%3$s)',
                $item->getPrice(),
                $item->getSinglePrice(),
                $item->getExtraPrice()
            );

            $result['extras'] = static::getItemCollectionRenderData(
                $item->getTabExtraItem()
            );
        }

        return $result;
    }



    /**
     * Get items render data.
     *
     * @param Item[]|ItemCollection $item
     * @return array
     */
    public static function getItemCollectionRenderData($item)
    {
        $result = array();
        $tabItem = (
            ($item instanceof ItemCollection) ?
                $item->getTabItem() :
                (is_array($item) ? $item : array())
        );

        foreach($tabItem as $item)
        {
            if($item instanceof Item)
            {
                $result[] = static::getItemRenderData($item);
            }
        }

        return $result;
    }
}