<?php
/**
 * Description :
 * This class allows to define item factory class.
 * 
 * @copyright Copyright (c) 2021 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace application\tracktik_test\electronic\factory;

use Exception;
use application\tracktik_test\electronic\model\MicrowaveItem;
use application\tracktik_test\electronic\model\ControllerItem;
use application\tracktik_test\electronic\model\ConsoleItem;
use application\tracktik_test\electronic\model\TelevisionItem;

class ItemFactory
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * Set check specified associative array of values,
     * following specified index array of keys, where check required.
     *
     * @param array $tabValue
     * @param array $tabKey = array()
     * @throws Exception
     */
    protected function setCheckValue(
        array $tabValue,
        array $tabKey = array()
    )
    {
        foreach($tabKey as $key)
        {
            if(!array_key_exists($key, $tabValue))
            {
                throw new Exception(sprintf(
                    'Following data "%1$s" not found, on values!',
                    $key
                ));
            }
        }
    }



    /**
     * Get new microwave item.
     *
     * @param array $tabValue
     * @return MicrowaveItem
     * @throws Exception
     */
    public function getMicrowaveItem(array $tabValue)
    {
        // Check values found
        $this->setCheckValue($tabValue, array('price'));

        $price = $tabValue['price'];
        return new MicrowaveItem($price);
    }



    /**
     * Get new controller item.
     *
     * @param array $tabValue
     * @return ControllerItem
     * @throws Exception
     */
    public function getControllerItem(array $tabValue)
    {
        // Check values found
        $this->setCheckValue($tabValue, array('price', 'wired'));

        $price = $tabValue['price'];
        $wired = $tabValue['wired'];
        return new ControllerItem($price, $wired);
    }



    /**
     * Get new console item.
     *
     * @param array $tabValue
     * @return ConsoleItem
     * @throws Exception
     */
    public function getConsoleItem(array $tabValue)
    {
        // Check values found
        $this->setCheckValue($tabValue, array('price', 'extra_item_collection'));

        $price = $tabValue['price'];
        $extraItemCollection = $tabValue['extra_item_collection'];
        return new ConsoleItem($price, $extraItemCollection);
    }



    /**
     * Get new television item.
     *
     * @param array $tabValue
     * @return TelevisionItem
     * @throws Exception
     */
    public function getTelevisionItem(array $tabValue)
    {
        // Check values found
        $this->setCheckValue($tabValue, array('price', 'extra_item_collection'));

        $price = $tabValue['price'];
        $extraItemCollection = $tabValue['extra_item_collection'];
        return new TelevisionItem($price, $extraItemCollection);
    }



    /**
     * Get new item.
     *
     * @param string $itemClassPath
     * @param array $tabValue
     * @return null|TelevisionItem
     * @throws Exception
     */
    public function getItem(
        $itemClassPath,
        array $tabValue
    )
    {
        $result = null;
        if(is_string($itemClassPath))
        {
            switch($itemClassPath)
            {
                case MicrowaveItem::class:
                    $result = $this->getMicrowaveItem($tabValue);
                    break;
                case ControllerItem::class:
                    $result = $this->getControllerItem($tabValue);
                    break;
                case ConsoleItem::class:
                    $result = $this->getConsoleItem($tabValue);
                    break;
                case TelevisionItem::class:
                    $result = $this->getTelevisionItem($tabValue);
                    break;
            }
        }

        // Check item can be found
        if(is_null($result))
        {
            throw new Exception(sprintf(
                'Following class path "%1$s" enable to instantiate valid item!',
                strval($itemClassPath)
            ));
        }

        return $result;
    }
}